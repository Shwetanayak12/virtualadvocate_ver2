USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[DocumentSubSubCategory]    Script Date: 10/05/2015 15:15:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DocumentSubSubCategory](
	[DocumentSubSubCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[SubSubCategoryDescription] [text] NULL,
	[SubDocumentCategoryName] [varchar](100) NULL,
	[DocumentSubCategoryId] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_SubSubCatrgory] PRIMARY KEY CLUSTERED 
(
	[DocumentSubSubCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[DocumentSubSubCategory]  WITH CHECK ADD  CONSTRAINT [FK_DocumentSubSubCatrgory_DocumentSubCategory] FOREIGN KEY([DocumentSubCategoryId])
REFERENCES [dbo].[DocumentSubCategory] ([DocumentSubCategoryId])
GO

ALTER TABLE [dbo].[DocumentSubSubCategory] CHECK CONSTRAINT [FK_DocumentSubSubCatrgory_DocumentSubCategory]
GO

ALTER TABLE [dbo].[DocumentSubSubCategory] ADD  CONSTRAINT [DF_SubSubCatrgory_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO


