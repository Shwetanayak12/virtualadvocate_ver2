USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[LogDueDiligenceUser]    Script Date: 2/5/2016 4:16:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LogDueDiligenceUser](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[Action] [varchar](50) NULL,
	[UserId] [int] NOT NULL,
	[ModifierId] [int] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[EmailAddress] [varchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[StreetName] [varchar](50) NULL,
	[BuildingName] [varchar](50) NULL,
	[PlotNumber] [varchar](50) NULL,
	[BlockNumber] [varchar](50) NULL,
	[Region] [varchar](50) NULL,
	[LandMark] [varchar](50) NULL,
	[IsEnabled] [bit] NULL,
	[EnquiryType] [int] NULL,
	[RoleId] [int] NULL,
 CONSTRAINT [PK_LogDueDiligenceUser] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


