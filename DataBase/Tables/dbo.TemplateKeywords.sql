USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[TemplateKeywords]    Script Date: 10/26/2015 11:46:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TemplateKeywords](
	[TemplateKeyId] [int] IDENTITY(1,1) NOT NULL,
	[TemplateKeyValue] [varchar](50) NOT NULL,
	[TemplateKeyLabels] [varchar](50) NOT NULL,
	[TemplateKeyDescription] [varchar](250) NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_TemplateKeywords] PRIMARY KEY CLUSTERED 
(
	[TemplateKeyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TemplateKeywords] ADD  CONSTRAINT [DF_TemplateKeywords_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO


