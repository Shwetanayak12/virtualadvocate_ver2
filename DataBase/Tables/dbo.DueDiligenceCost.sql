USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[DueDiligenceCost]    Script Date: 2/5/2016 4:13:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DueDiligenceCost](
	[CostId] [int] IDENTITY(1,1) NOT NULL,
	[DueDiligenceType] [varchar](50) NULL,
	[Cost] [money] NULL,
	[IsEnabled] [bit] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_DueDiligenceCost] PRIMARY KEY CLUSTERED 
(
	[CostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[DueDiligenceCost] ADD  CONSTRAINT [DF_DueDiligenceCost_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO


