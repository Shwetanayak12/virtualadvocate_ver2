USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[DocumentTemplate]    Script Date: 10/21/2015 15:42:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DocumentTemplate](
	[TemplateId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentType] [varchar](100) NULL,
	[DocumentTitle] [varchar](100) NULL,
	[DocumentDescription] [varchar](250) NULL,
	[TemplateCost] [money] NULL,
	[TemplateFileName] [varchar](200) NULL,
	[DocumentCategory] [int] NOT NULL,
	[DocumentSubCategory] [int] NULL,
	[DocumentSubSubCategory] [int] NULL,
	[IsEnabled] [bit] NOT NULL,
	[AssociateTemplateId] [int] NULL,
 CONSTRAINT [PK_DocumentTemplate] PRIMARY KEY CLUSTERED 
(
	[TemplateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[DocumentTemplate] ADD  CONSTRAINT [DF_DocumentTemplate_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO


