USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[DocumentSubCategory]    Script Date: 10/05/2015 15:15:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DocumentSubCategory](
	[DocumentSubCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentCategoryId] [int] NOT NULL,
	[DocumentSubCategoryName] [varchar](100) NULL,
	[SubCategoryDescription] [text] NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_SubCategory] PRIMARY KEY CLUSTERED 
(
	[DocumentSubCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[DocumentSubCategory]  WITH CHECK ADD  CONSTRAINT [FK_DocumentSubCategory_DocumentCategory] FOREIGN KEY([DocumentCategoryId])
REFERENCES [dbo].[DocumentCategory] ([DocumentCategoryId])
GO

ALTER TABLE [dbo].[DocumentSubCategory] CHECK CONSTRAINT [FK_DocumentSubCategory_DocumentCategory]
GO

ALTER TABLE [dbo].[DocumentSubCategory] ADD  CONSTRAINT [DF_SubCategory_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO


