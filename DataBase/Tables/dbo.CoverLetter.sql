USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[CoverLetter]    Script Date: 11/04/2015 12:37:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CoverLetter](
	[CoverLetterId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[CustomerName] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[BankAddress] [text] NULL,
	[CreatedDate] [datetime] NULL,
	[IsEnabled] [bit] NULL,
 CONSTRAINT [PK_CoverLetter] PRIMARY KEY CLUSTERED 
(
	[CoverLetterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CoverLetter] ADD  CONSTRAINT [DF_CoverLetter_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[CoverLetter] ADD  CONSTRAINT [DF_CoverLetter_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO


