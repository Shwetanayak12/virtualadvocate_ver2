USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[DocumentCategory]    Script Date: 10/05/2015 15:13:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DocumentCategory](
	[DocumentCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentCategoryDescription] [text] NULL,
	[DocumentCategoryName] [varchar](100) NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_DocumentCategory] PRIMARY KEY CLUSTERED 
(
	[DocumentCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[DocumentCategory] ADD  CONSTRAINT [DF_DocumentCategory_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO


