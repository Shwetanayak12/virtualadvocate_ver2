USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[TemplateKeysPointer]    Script Date: 10/14/2015 18:45:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TemplateKeysPointer](
	[TemplateKeyRowId] [int] IDENTITY(1,1) NOT NULL,
	[TemplateId] [int] NOT NULL,
	[TemplateKeyId] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_TemplateKeysPointer] PRIMARY KEY CLUSTERED 
(
	[TemplateKeyRowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TemplateKeysPointer] ADD  CONSTRAINT [DF_TemplateKeysPointer_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO


