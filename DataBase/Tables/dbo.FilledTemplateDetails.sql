USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[FilledTemplateDetails]    Script Date: 11/04/2015 12:37:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FilledTemplateDetails](
	[RowId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TemplateId] [int] NOT NULL,
	[FilledTemplateName] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[PaidStatus] [bit] NOT NULL,
	[Amount] [decimal](18, 0) NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [PK_FilledTemplates] PRIMARY KEY CLUSTERED 
(
	[RowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FilledTemplateDetails] ADD  CONSTRAINT [DF_FilledTemplates_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[FilledTemplateDetails] ADD  CONSTRAINT [DF_FilledTemplates_PaidStatus]  DEFAULT ((0)) FOR [PaidStatus]
GO


