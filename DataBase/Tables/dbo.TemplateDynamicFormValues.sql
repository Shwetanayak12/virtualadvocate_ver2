USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[TemplateDynamicFormValues]    Script Date: 10/21/2015 15:43:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TemplateDynamicFormValues](
	[RowId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TemplateId] [int] NOT NULL,
	[TemplateKey] [varchar](50) NOT NULL,
	[UserInputs] [varchar](100) NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_TemplateDynamicFormValues] PRIMARY KEY CLUSTERED 
(
	[RowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TemplateDynamicFormValues] ADD  CONSTRAINT [DF_TemplateDynamicFormValues_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO


