USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[ApplicationConfiguration]    Script Date: 2/16/2016 1:03:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ApplicationConfiguration](
	[KeyId] [int] IDENTITY(1,1) NOT NULL,
	[KeyName] [varchar](50) NULL,
	[KeyValue] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[IsEnabled] [bit] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ApplicationConfiguration] ADD  CONSTRAINT [DF_ApplicationConfiguration_IsEnabled]  DEFAULT ((0)) FOR [IsEnabled]
GO


