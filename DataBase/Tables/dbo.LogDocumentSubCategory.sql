USE [VirtualAdvocate]
GO

/****** Object:  Table [dbo].[LogDocumentSubCategory]    Script Date: 2/5/2016 4:15:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LogDocumentSubCategory](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[Action] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[DocumentSubCategoryId] [int] NULL,
	[DocumentCategoryId] [int] NULL,
	[DocumentSubCategoryName] [varchar](50) NULL,
	[SubCategoryDescription] [varchar](50) NULL,
	[IsEnabled] [bit] NULL,
 CONSTRAINT [PK_LogDocumentSubCategory] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


