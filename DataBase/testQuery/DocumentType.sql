CREATE PROCEDURE GetCategoriesMonthCount_sp
@OrgId INT=NULL,
@UserId INT=NULL
AS
BEGIN
IF @OrgId IS NOT NULL AND @UserId IS NOT NULL
SELECT COUNT(*) AS MonthlyCount FROM FilledTemplateDetails
WHERE CreatedDate <= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND CreatedDate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
AND OrgId=@OrgId 
ELSE IF @UserId IS NOT NULL
SELECT COUNT(*) AS MonthlyCount FROM FilledTemplateDetails
WHERE CreatedDate <= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND CreatedDate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
AND UserId=@UserId
ELSE
SELECT COUNT(*) AS MonthlyCount FROM FilledTemplateDetails
WHERE CreatedDate <= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND CreatedDate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
AND UserId=@UserId
END

SELECT COUNT(FT.TemplateId) AS MonthlyCount, DC.DocumentCategoryName FROM FilledTemplateDetails FT
JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
--WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
GROUP BY DC.DocumentCategoryName
--	sdq      AND UserId=@UserId

Select  * from FilledTemplateDetails order by createddate desc
select * from DocumentCategory
select * from DocumentTemplate

print DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0)
print DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)

WITH CTE(TemplateId,DocumentCategoryName,CreatedDate)--,DocumentCategory,DocumentSubCategory,DocumentSubSubCategory,DocumentSubCategoryName,SubDocumentCategoryName)
AS(
SELECT  FT.TemplateId,DC.DocumentCategoryName,FT.CreatedDate--,DT.DocumentCategory,DT.DocumentSubCategory,DT.DocumentSubSubCategory,DSC.DocumentSubCategoryName,DSS.SubDocumentCategoryName 
FROM FilledTemplateDetails FT
JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
--RIGHT JOIN DocumentSubCategory DSC ON DC.DocumentCategoryId=DSC.DocumentCategoryId
--LEFT JOIN DocumentSubSubCategory DSS ON DSC.DocumentSubCategoryId=DSS.DocumentSubCategoryId
--WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
)
SELECT DocumentCategoryName,COUNT(TemplateId) AS COUNT  FROM CTE
WHERE CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
GROUP BY DocumentCategoryName


Select * from OrganizationDetails

select * from OrganizationType
GetTotalDocumentCountByOrganization_sp
[dbo].[DueDiligenceEnquiry]
select * from DueDiligenceEnquiry
select * from userprofile
select * from roles

