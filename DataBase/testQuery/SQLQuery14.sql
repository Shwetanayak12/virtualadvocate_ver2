USE [VirtualAdvocate]
GO
/****** Object:  StoredProcedure [dbo].[GetCategoriesOrgUsersCurrnetMonthTotalCount]    Script Date: 1/14/2016 2:52:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCategoriesOrgUsersCurrnetMonthTotalCount]
@CategoryType INT=NULL ,
@OrgId INT=NULL,
@UserId INT
AS
BEGIN
IF @CategoryType=1

BEGIN
	
		IF @OrgId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
			WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
			AND FT.OrgId=@OrgId
			GROUP BY DocumentCategoryName

			ELSE

			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
			WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
			AND FT.UserId=@UserId
			GROUP BY DocumentCategoryName
END
	

ELSE IF @CategoryType=2
	
	BEGIN
		IF @OrgId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
			WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
			AND FT.OrgId=@OrgId
			GROUP BY DocumentSubCategoryName
		ELSE
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
			WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
			AND FT.UserId=@UserId
			GROUP BY DocumentSubCategoryName
	END
	

ELSE 
	
	BEGIN
		IF @OrgId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
			WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
			AND FT.OrgId=@OrgId
			GROUP BY SubDocumentCategoryName
		ELSE
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
			WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
			AND FT.UserId=@UserId
			GROUP BY SubDocumentCategoryName
	END
	

END
GO
/****** Object:  StoredProcedure [dbo].[GetCategoriesOrgUsersTotalCount]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCategoriesOrgUsersTotalCount]
@CategoryType INT=NULL, 
@OrgId INT=NULL,
@UserId INT
AS
BEGIN
IF @CategoryType=1
	BEGIN
		
		IF @OrgId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId			
			AND FT.OrgId=@OrgId
			GROUP BY DocumentCategoryName
		ELSE IF @UserId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId			
			AND FT.UserId=@UserId
			GROUP BY DocumentCategoryName
		ELSE
		SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId	
			GROUP BY DocumentCategoryName
	END
	
ELSE IF @CategoryType=2
	BEGIN
		IF @OrgId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId			
			AND FT.OrgId=@OrgId
			GROUP BY DocumentSubCategoryName
		ELSE IF @UserId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId		
			AND FT.UserId=@UserId
			GROUP BY DocumentSubCategoryName
		ELSE
		SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
			GROUP BY DocumentSubCategoryName

	END
ELSE 
	BEGIN
		IF @OrgId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId			
			AND FT.OrgId=@OrgId
			GROUP BY SubDocumentCategoryName
		ELSE IF @UserId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId			
			AND FT.UserId=@UserId
			GROUP BY SubDocumentCategoryName
		ELSE
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
			GROUP BY SubDocumentCategoryName
	END
END
GO
/****** Object:  StoredProcedure [dbo].[GetCategoriesTotalCount]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCategoriesTotalCount]
@CategoryType INT=NULL 
AS
BEGIN
IF @CategoryType=1
	SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
	FROM FilledTemplateDetails FT
	JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
	RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
	GROUP BY DocumentCategoryName
ELSE IF @CategoryType=2
	SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
	FROM FilledTemplateDetails FT
	JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
	RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
	GROUP BY DocumentSubCategoryName
ELSE 
	SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
	FROM FilledTemplateDetails FT
	JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
	RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
	GROUP BY SubDocumentCategoryName
END
GO
/****** Object:  StoredProcedure [dbo].[GetCurrentMonthCount_sp]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCurrentMonthCount_sp]
@OrgId INT=NULL,
@UserId INT=NULL
AS
BEGIN
IF @OrgId IS NOT NULL
SELECT COUNT(*) AS MonthlyCount FROM FilledTemplateDetails
WHERE CreatedDate <= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND CreatedDate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
AND OrgId=@OrgId 
ELSE  
SELECT COUNT(*) AS MonthlyCount FROM FilledTemplateDetails
WHERE CreatedDate <= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND CreatedDate > DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
AND UserId=@UserId
END
GO
/****** Object:  StoredProcedure [dbo].[GetOrganizationCountByCategoy]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetOrganizationCountByCategoy]
AS
BEGIN
SELECT OT.OrganizationType ,COUNT(OD.OrganizationTypeId) AS NewCount FROM OrganizationDetails OD
RIGHT JOIN OrganizationType OT ON OD.OrganizationTypeId=OT.OrganizationTypeId
GROUP BY OrganizationType
END

GO
/****** Object:  StoredProcedure [dbo].[GetorganizationUserList_sp]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetorganizationUserList_sp]
@OrgId INT
AS
BEGIN
SELECT up.UserID,(ua.FirstName+' '+ua.LastName) AS Name
FROM UserProfile up
JOIN UserAddressDetails ua ON up.UserID=ua.UserId
WHERE up.OrganizationId=@OrgId
END

GO
/****** Object:  StoredProcedure [dbo].[GetTotalDocumentCountByOrganization_sp]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTotalDocumentCountByOrganization_sp]
AS
BEGIN
SELECT OD.OrgName,COUNT(FT.OrgId) AS TotalDocumentCount FROM FilledTemplateDetails FT
RIGHT JOIN OrganizationDetails OD ON FT.OrgId=OD.OrganizationId
GROUP BY OrgName ORDER BY TotalDocumentCount DESC
END


GO
/****** Object:  StoredProcedure [dbo].[GetTotalDocumentCountOrgUser_sp]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTotalDocumentCountOrgUser_sp]
@OrgId INT=NULL,
@UserId INT=NULL
AS
BEGIN
IF @OrgId IS NOT NULL
SELECT COUNT(*) AS TotalCount FROM FilledTemplateDetails
WHERE  OrgId=@OrgId 
ELSE  
SELECT COUNT(*) AS TotalCount FROM FilledTemplateDetails
WHERE UserId=@UserId
END
GO
/****** Object:  StoredProcedure [dbo].[InvoiceList_sp]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InvoiceList_sp]
AS
BEGIN
select  ISNULL(b.PaidStatus,0) As PaidStatus, b.InvoiceDocumentName,c.DocumentTitle ,a.CreatedDate,
a.CustomerId,a.groupid, d.CustomerName

from FilledTemplateDetails a left join InvoiceDetails b
on a.CustomerId=b.CustomerId and a.GroupId=b.GroupId
join DocumentTemplate c on a.TemplateId=c.TemplateId
join CustomerDetails d on a.CustomerId=d.CustomerId
order by a.RowId desc
END
GO
/****** Object:  StoredProcedure [dbo].[InvoiceListAcAdmin_sp]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InvoiceListAcAdmin_sp]
@OrgId INT=NULL,
@UserId INT=NULL
AS
BEGIN

IF @OrgId IS NOT NULL
	SELECT  ISNULL(b.PaidStatus,0) As PaidStatus, b.InvoiceDocumentName,c.DocumentTitle ,a.CreatedDate,
	a.CustomerId,a.groupid, d.CustomerName
	FROM FilledTemplateDetails a left join InvoiceDetails b
	ON a.CustomerId=b.CustomerId and a.GroupId=b.GroupId
	JOIN DocumentTemplate c on a.TemplateId=c.TemplateId
	JOIN CustomerDetails d on a.CustomerId=d.CustomerId
	WHERE a.OrgId=@OrgId
	ORDER BY a.RowId desc
ELSE
	SELECT  ISNULL(b.PaidStatus,0) As PaidStatus, b.InvoiceDocumentName,c.DocumentTitle ,a.CreatedDate,
	a.CustomerId,a.groupid, d.CustomerName
	FROM FilledTemplateDetails a left join InvoiceDetails b
	ON a.CustomerId=b.CustomerId and a.GroupId=b.GroupId
	JOIN DocumentTemplate c on a.TemplateId=c.TemplateId
	JOIN CustomerDetails d on a.CustomerId=d.CustomerId
	WHERE a.UserId=@UserId
	ORDER BY a.RowId DESC
END
GO
/****** Object:  StoredProcedure [dbo].[ReportsGenerate]    Script Date: 1/14/2016 2:52:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ReportsGenerate]
@RoleId INT,
@ReportType INT,
@FromDate DATETIME = NULL,
@ToDate DATETIME = NULL,
@OrgId INT = NULL,
@UserId INT = NULL,
@DocumentTypeId INT = NULL,
@DocumentSubId INT=NULL,
@DocumentSubSubId INT=NULL
AS
BEGIN

DECLARE @Filter VARCHAR(250)
DECLARE @Filter1 VARCHAR(2500)
DECLARE @Filter2 VARCHAR(250)
SET @Filter=' Where '
SET @Filter2=' order by a.RowId desc '
IF @ReportType=1 
BEGIN
SET @Filter=@Filter + ' a.CreatedDate Between '''+CONVERT(varchar, @FromDate,113)+''' and '''+ CONVERT(varchar, @ToDate,113) +''''
	IF @RoleId=2
	SET @Filter=@Filter + 'and a.OrgId= '+CONVERT(varchar,@OrgId)
	IF @RoleId=5 OR @RoleId=3
	SET @Filter=@Filter + 'and a.userid= '+CONVERT(varchar,@UserId)

END
ELSE IF @ReportType=2 
BEGIN
SET @Filter=@Filter + 'a.OrgId= '+CONVERT(varchar,@OrgId)
END
ELSE IF @ReportType=3 
BEGIN
	SET @Filter=@Filter + 'a.userid= '+CONVERT(varchar,@UserId)
END

ELSE IF @ReportType=4 
BEGIN
	IF @DocumentTypeId IS NOT NULL
	BEGIN
	SET @Filter=@Filter+'c.DocumentCategory='+CONVERT(varchar,@DocumentTypeId)
	IF @DocumentSubId IS NOT NULL
	SET @Filter=@Filter+' and c.DocumentSubCategory='+CONVERT(varchar,@DocumentSubId)
	IF @DocumentSubSubId IS NOT NULL
	SET @Filter=@Filter+' and c.DocumentSubSubCategory='+CONVERT(varchar,@DocumentSubSubId)
	IF @RoleId=5 OR @RoleId=3
	SET @Filter=@Filter + ' and a.userid= '+CONVERT(varchar,@UserId)
	IF @RoleId=2
	SET @Filter=@Filter + ' and a.OrgId= '+CONVERT(varchar,@OrgId)
	END
END
ELSE
SET @Filter=''

SET @Filter1 = 'select  c.DocumentTitle ,c.DocumentType,
 d.CustomerName,org.OrgName,a.CreatedDate, dc.DocumentCategoryName,dsc.DocumentSubCategoryName,dss.SubDocumentCategoryName,
 a.UserId,a.CustomerId,a.groupid

from FilledTemplateDetails a left join InvoiceDetails b
on a.CustomerId=b.CustomerId and a.GroupId=b.GroupId
join DocumentTemplate c on a.TemplateId=c.TemplateId
join CustomerDetails d on a.CustomerId=d.CustomerId
join UserProfile up on a.UserId=up.UserId
left join OrganizationDetails org on a.OrgId=org.OrganizationId 
join DocumentCategory dc on c.DocumentCategory= dc.DocumentCategoryId
join DocumentSubCategory dsc on c.DocumentSubCategory=dsc.DocumentSubCategoryId
join DocumentSubSubCategory dss on c.DocumentSubSubCategory=dss.DocumentSubSubCategoryId' +@Filter + @Filter2

--print @Filter1

exec(@Filter1)


END



GO
