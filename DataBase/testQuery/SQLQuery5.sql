select  c.DocumentTitle ,c.DocumentType,
 d.CustomerName,org.OrgName,a.CreatedDate,a.UserId,a.CustomerId,a.groupid,
 dc.DocumentCategoryName,dsc.DocumentSubCategoryName,dss.SubDocumentCategoryName


from FilledTemplateDetails a left join InvoiceDetails b
on a.CustomerId=b.CustomerId and a.GroupId=b.GroupId
join DocumentTemplate c on a.TemplateId=c.TemplateId
join CustomerDetails d on a.CustomerId=d.CustomerId
join UserProfile up on a.UserId=up.UserId
left join OrganizationDetails org on a.OrgId=org.OrganizationId 
join DocumentCategory dc on c.DocumentCategory= dc.DocumentCategoryId
join DocumentSubCategory dsc on c.DocumentSubCategory=dsc.DocumentSubCategoryId
join DocumentSubSubCategory dss on c.DocumentSubSubCategory=dss.DocumentSubSubCategoryId

 Where a.OrgId = 4 order by a.RowId desc 