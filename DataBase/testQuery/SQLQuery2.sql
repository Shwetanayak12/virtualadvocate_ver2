ALTER PROCEDURE ReportsGenerate
@RoleId INT,
@ReportType INT,
@FromDate DATETIME = NULL,
@ToDate DATETIME = NULL,
@OrgId INT = NULL,
@UserId INT = NULL,
@DocumentTypeId INT = NULL,
@DocumentSubId INT=NULL,
@DocumentSubSubId INT=NULL
AS
BEGIN

DECLARE @Filter VARCHAR(250)
DECLARE @Filter1 VARCHAR(2500)
DECLARE @Filter2 VARCHAR(250)
SET @Filter=' Where '
SET @Filter2=' order by a.RowId desc '
IF @ReportType=1 
BEGIN
SET @Filter=@Filter + ' a.CreatedDate Between '''+CONVERT(varchar, @FromDate,113)+''' and '''+ CONVERT(varchar, @ToDate,113) +''''
	IF @RoleId=2
	SET @Filter=@Filter + 'and a.OrgId= '+CONVERT(varchar,@OrgId)
	IF @RoleId=5 OR @RoleId=3
	SET @Filter=@Filter + 'and a.userid= '+CONVERT(varchar,@UserId)

END
ELSE IF @ReportType=2 
BEGIN
SET @Filter=@Filter + 'a.OrgId= '+CONVERT(varchar,@OrgId)
END
ELSE IF @ReportType=3 
BEGIN
	SET @Filter=@Filter + 'a.userid= '+CONVERT(varchar,@UserId)
END

ELSE IF @ReportType=4 
BEGIN
	IF @DocumentTypeId IS NOT NULL
	BEGIN
	SET @Filter=@Filter+'c.DocumentCategory='+CONVERT(varchar,@DocumentTypeId)
	IF @DocumentSubId IS NOT NULL
	SET @Filter=@Filter+' and c.DocumentSubCategory='+CONVERT(varchar,@DocumentSubId)
	IF @DocumentSubSubId IS NOT NULL
	SET @Filter=@Filter+' and c.DocumentSubSubCategory='+CONVERT(varchar,@DocumentSubSubId)
	IF @RoleId=5 OR @RoleId=3
	SET @Filter=@Filter + ' and a.userid= '+CONVERT(varchar,@UserId)
	IF @RoleId=2
	SET @Filter=@Filter + ' and a.OrgId= '+CONVERT(varchar,@OrgId)
	END
END
ELSE
SET @Filter=''

SET @Filter1 = 'select  c.DocumentTitle ,c.DocumentType,
 d.CustomerName,org.OrgName,a.CreatedDate, dc.DocumentCategoryName,dsc.DocumentSubCategoryName,dss.SubDocumentCategoryName,
 a.UserId,a.CustomerId,a.groupid

from FilledTemplateDetails a left join InvoiceDetails b
on a.CustomerId=b.CustomerId and a.GroupId=b.GroupId
join DocumentTemplate c on a.TemplateId=c.TemplateId
join CustomerDetails d on a.CustomerId=d.CustomerId
join UserProfile up on a.UserId=up.UserId
left join OrganizationDetails org on a.OrgId=org.OrganizationId 
join DocumentCategory dc on c.DocumentCategory= dc.DocumentCategoryId
join DocumentSubCategory dsc on c.DocumentSubCategory=dsc.DocumentSubCategoryId
join DocumentSubSubCategory dss on c.DocumentSubSubCategory=dss.DocumentSubSubCategoryId' +@Filter + @Filter2

print @Filter1

exec(@Filter1)


END


