﻿SET IDENTITY_INSERT [dbo].[OrganizationType] ON
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [OrganizationType], [OrganizationDescription], [IsEnabled]) VALUES (1, N'Bank', N'Banking Information', 1)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [OrganizationType], [OrganizationDescription], [IsEnabled]) VALUES (2, N'Law Firm', N'Law Firm', 1)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [OrganizationType], [OrganizationDescription], [IsEnabled]) VALUES (3, N'Human Resources', N'Human Resources', 1)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [OrganizationType], [OrganizationDescription], [IsEnabled]) VALUES (4, N'Real Estate', N'Real Estate', 1)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [OrganizationType], [OrganizationDescription], [IsEnabled]) VALUES (5, N'Individual', N'Individual', 1)
INSERT INTO [dbo].[OrganizationType] ([OrganizationTypeId], [OrganizationType], [OrganizationDescription], [IsEnabled]) VALUES (6, N'Other', N'Other', 1)
SET IDENTITY_INSERT [dbo].[OrganizationType] OFF
