SET IDENTITY_INSERT [dbo].[DueDiligenceCost] ON
INSERT INTO [dbo].[DueDiligenceCost] ([CostId], [DueDiligenceType], [Cost], [IsEnabled], [CreatedDate]) VALUES (1, N'1', CAST(150000.0000 AS Money), 1, N'2016-02-06 09:36:43')
INSERT INTO [dbo].[DueDiligenceCost] ([CostId], [DueDiligenceType], [Cost], [IsEnabled], [CreatedDate]) VALUES (2, N'2', CAST(150000.0000 AS Money), 1, N'2016-02-06 09:36:59')
INSERT INTO [dbo].[DueDiligenceCost] ([CostId], [DueDiligenceType], [Cost], [IsEnabled], [CreatedDate]) VALUES (3, N'3', CAST(250000.0000 AS Money), 1, N'2016-02-06 09:37:21')
SET IDENTITY_INSERT [dbo].[DueDiligenceCost] OFF