﻿SET IDENTITY_INSERT [dbo].[Roles] ON
INSERT INTO [dbo].[Roles] ([RoleId], [RoleDescription]) VALUES (1, N'Super Admin')
INSERT INTO [dbo].[Roles] ([RoleId], [RoleDescription]) VALUES (2, N'Account Admin')
INSERT INTO [dbo].[Roles] ([RoleId], [RoleDescription]) VALUES (3, N'Individual User')
INSERT INTO [dbo].[Roles] ([RoleId], [RoleDescription]) VALUES (4, N'Due Diligence User')
INSERT INTO [dbo].[Roles] ([RoleId], [RoleDescription]) VALUES (5, N'Company User')
SET IDENTITY_INSERT [dbo].[Roles] OFF
