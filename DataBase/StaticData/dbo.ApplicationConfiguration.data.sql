﻿SET IDENTITY_INSERT [dbo].[ApplicationConfiguration] ON
INSERT INTO [dbo].[ApplicationConfiguration] ([KeyId], [KeyName], [KeyValue], [CreatedDate], [IsEnabled]) VALUES (1, N'AdminMailAddress', N'lenin.r@flatworldsolutions.com', N'2016-02-16 00:00:00', 1)
INSERT INTO [dbo].[ApplicationConfiguration] ([KeyId], [KeyName], [KeyValue], [CreatedDate], [IsEnabled]) VALUES (2, N'DueDays', N'5', N'2016-02-16 00:00:00', 1)
INSERT INTO [dbo].[ApplicationConfiguration] ([KeyId], [KeyName], [KeyValue], [CreatedDate], [IsEnabled]) VALUES (3, N'VAT', N'18', N'2016-02-16 00:00:00', 1)
INSERT INTO [dbo].[ApplicationConfiguration] ([KeyId], [KeyName], [KeyValue], [CreatedDate], [IsEnabled]) VALUES (4, N'ApplicationName', N'Virtual Advocate', N'2016-02-16 00:00:00', 1)
INSERT INTO [dbo].[ApplicationConfiguration] ([KeyId], [KeyName], [KeyValue], [CreatedDate], [IsEnabled]) VALUES (5, N'ApplicationUrlEncryptionKey', N'!#$a54?3', N'2016-02-16 00:00:00', 1)
SET IDENTITY_INSERT [dbo].[ApplicationConfiguration] OFF
