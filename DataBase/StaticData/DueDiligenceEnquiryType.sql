SET IDENTITY_INSERT [dbo].[DueDiligenceEnquiryType] ON
INSERT INTO [dbo].[DueDiligenceEnquiryType] ([EnquiryTypeId], [EnquiryType], [IsEnabled]) VALUES (1, N'Business', 1)
INSERT INTO [dbo].[DueDiligenceEnquiryType] ([EnquiryTypeId], [EnquiryType], [IsEnabled]) VALUES (2, N'Company', 1)
INSERT INTO [dbo].[DueDiligenceEnquiryType] ([EnquiryTypeId], [EnquiryType], [IsEnabled]) VALUES (3, N'Land', 1)
SET IDENTITY_INSERT [dbo].[DueDiligenceEnquiryType] OFF