﻿SET IDENTITY_INSERT [dbo].[UserAccountType] ON
INSERT INTO [dbo].[UserAccountType] ([UserAccountTypeId], [UserAccountDescription]) VALUES (1, N'Single User Account')
INSERT INTO [dbo].[UserAccountType] ([UserAccountTypeId], [UserAccountDescription]) VALUES (2, N'Multiple User Account')
SET IDENTITY_INSERT [dbo].[UserAccountType] OFF
