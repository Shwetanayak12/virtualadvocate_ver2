﻿SET IDENTITY_INSERT [dbo].[AccountServices] ON
INSERT INTO [dbo].[AccountServices] ([ServiceId], [ServicesDescription], [IsEnabled]) VALUES (1, N'Human Resources', 1)
INSERT INTO [dbo].[AccountServices] ([ServiceId], [ServicesDescription], [IsEnabled]) VALUES (2, N'Banks', 1)
INSERT INTO [dbo].[AccountServices] ([ServiceId], [ServicesDescription], [IsEnabled]) VALUES (3, N'Law Firms', 1)
INSERT INTO [dbo].[AccountServices] ([ServiceId], [ServicesDescription], [IsEnabled]) VALUES (4, N'Real Estate', 1)
SET IDENTITY_INSERT [dbo].[AccountServices] OFF
