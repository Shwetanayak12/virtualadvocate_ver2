﻿SET IDENTITY_INSERT [dbo].[ReportTypes] ON
INSERT INTO [dbo].[ReportTypes] ([ReportTypeId], [ReportTypeName], [IsEnabled]) VALUES (1, N'Date', 1)
INSERT INTO [dbo].[ReportTypes] ([ReportTypeId], [ReportTypeName], [IsEnabled]) VALUES (2, N'Company', 1)
INSERT INTO [dbo].[ReportTypes] ([ReportTypeId], [ReportTypeName], [IsEnabled]) VALUES (3, N'User', 1)
INSERT INTO [dbo].[ReportTypes] ([ReportTypeId], [ReportTypeName], [IsEnabled]) VALUES (4, N'DocumentType', 1)
SET IDENTITY_INSERT [dbo].[ReportTypes] OFF
