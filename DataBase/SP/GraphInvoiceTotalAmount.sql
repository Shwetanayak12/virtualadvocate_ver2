CREATE PROCEDURE GraphInvoiceTotalAmount
@UserId INT = NULL
AS
BEGIN
DECLARE @Orgid INT
IF @UserId IS NOT NULL
BEGIN
SET @Orgid=(SELECT OrganizationId FROM UserProfile WHERE UserId=@UserId)

	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DATENAME(MONTH, GETDATE()) As MnthName FROM InvoiceDetails
	WHERE Customerid IN(Select Customerid from CustomerDetails where OrganizationId=@Orgid)
	AND DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, 0, getdate()))
	UNION
	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DateName( month , DateAdd( month , DATEPART(m, DATEADD(m, -1, getdate())) , 0 ) - 1 ) AS MnthName FROM InvoiceDetails
	WHERE Customerid IN(Select Customerid from CustomerDetails where OrganizationId=@Orgid)
	AND DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, -1, getdate()))
	UNION
	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DateName( month , DateAdd( month , DATEPART(m, DATEADD(m, -2, getdate())) , 0 ) - 1 ) AS MnthName FROM InvoiceDetails
	WHERE Customerid IN(Select Customerid from CustomerDetails where OrganizationId=@Orgid)
	AND DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, -2, getdate()))
	UNION
	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DateName( month , DateAdd( month , DATEPART(m, DATEADD(m, -3, getdate())) , 0 ) - 1 ) AS MnthName FROM InvoiceDetails
	WHERE Customerid IN(Select Customerid from CustomerDetails where OrganizationId=@Orgid)
	AND DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, -3, getdate()))
	UNION
	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DateName( month , DateAdd( month , DATEPART(m, DATEADD(m, -4, getdate())) , 0 ) - 1 ) AS MnthName FROM InvoiceDetails
	WHERE Customerid IN(Select Customerid from CustomerDetails where OrganizationId=@Orgid)
	AND DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, -4, getdate()))

END
ELSE
BEGIN

	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DATENAME(MONTH, GETDATE()) As MnthName FROM InvoiceDetails
	WHERE DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, 0, getdate()))
	UNION
	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DateName( month , DateAdd( month , DATEPART(m, DATEADD(m, -1, getdate())) , 0 ) - 1 ) AS MnthName FROM InvoiceDetails
	WHERE DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, -1, getdate()))
	UNION
	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DateName( month , DateAdd( month , DATEPART(m, DATEADD(m, -2, getdate())) , 0 ) - 1 ) AS MnthName FROM InvoiceDetails
	WHERE DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, -2, getdate()))
	UNION
	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DateName( month , DateAdd( month , DATEPART(m, DATEADD(m, -3, getdate())) , 0 ) - 1 ) AS MnthName FROM InvoiceDetails
	WHERE DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, -3, getdate()))
	UNION
	SELECT ISNULL(SUM(TotalAmount),0) AS  Amount,DateName( month , DateAdd( month , DATEPART(m, DATEADD(m, -4, getdate())) , 0 ) - 1 ) AS MnthName FROM InvoiceDetails
	WHERE DATEPART(m, CreatedDate) = DATEPART(m, DATEADD(m, -4, getdate()))

END

END


