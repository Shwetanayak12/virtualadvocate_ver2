ALTER PROCEDURE ViewLogSubCategory
@LogId INT
AS
BEGIN

IF EXISTS(SELECT LogId FROM LogDocumentSubCategory WHERE LogId<@LogId)
BEGIN
	SELECT [Action],ModifiedDate,lc.DocumentCategoryId,SubCategoryDescription,DocumentSubCategoryName,lc.IsEnabled,DocumentCategoryName FROM
	LogDocumentSubCategory lc JOIN DocumentCategory dc ON lc.DocumentCategoryId=dc.DocumentCategoryId
	WHERE LogId=@LogId 
	UNION
	SELECT TOP 1 [Action],ModifiedDate,lc.DocumentCategoryId,SubCategoryDescription,DocumentSubCategoryName,lc.IsEnabled,DocumentCategoryName FROM
	LogDocumentSubCategory lc JOIN DocumentCategory dc ON lc.DocumentCategoryId=dc.DocumentCategoryId
	WHERE LogId<@LogId 
END
ELSE
SELECT [Action],ModifiedDate,lc.DocumentCategoryId,SubCategoryDescription,DocumentSubCategoryName,lc.IsEnabled,DocumentCategoryName FROM
	LogDocumentSubCategory lc JOIN DocumentCategory dc ON lc.DocumentCategoryId=dc.DocumentCategoryId
	WHERE LogId=@LogId 
END


