  CREATE PROCEDURE LogDueRegistrationList
  AS
  BEGIN
  SELECT [Action],ModifiedDate,FirstName+' '+LastName As Name, LR.RoleId,RoleDescription,LR.LogId FROM LogDueDiligenceUser LR  
  JOIN Roles ON LR.RoleId=Roles.RoleId ORDER BY ModifiedDate DESC
  END
