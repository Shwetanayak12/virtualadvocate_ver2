USE [VirtualAdvocate]
GO
ALTER PROCEDURE GetCategoriesOrgUsersTotalCount
@CategoryType INT=NULL, 
@OrgId INT=NULL,
@UserId INT = NULL
AS
BEGIN
IF @CategoryType=1
	BEGIN
		
		IF @OrgId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId			
			AND FT.OrgId=@OrgId
			GROUP BY DocumentCategoryName
		ELSE IF @UserId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId			
			AND FT.UserId=@UserId
			GROUP BY DocumentCategoryName
		ELSE
		SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId	
			GROUP BY DocumentCategoryName
	END
	
ELSE IF @CategoryType=2
	BEGIN
		IF @OrgId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId			
			AND FT.OrgId=@OrgId
			GROUP BY DocumentSubCategoryName
		ELSE IF @UserId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId		
			AND FT.UserId=@UserId
			GROUP BY DocumentSubCategoryName
		ELSE
		SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
			GROUP BY DocumentSubCategoryName

	END
ELSE 
	BEGIN
		IF @OrgId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId			
			AND FT.OrgId=@OrgId
			GROUP BY SubDocumentCategoryName
		ELSE IF @UserId IS NOT NULL
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId			
			AND FT.UserId=@UserId
			GROUP BY SubDocumentCategoryName
		ELSE
			SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
			GROUP BY SubDocumentCategoryName
	END
END


