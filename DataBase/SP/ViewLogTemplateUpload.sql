CREATE PROCEDURE ViewLogTemplateUpload 
@LogId INT
AS
BEGIN

IF EXISTS(SELECT LogId FROM LogTemplateUpload WHERE LogId<@LogId)
BEGIN
	SELECT [Action],ModifiedDate,DocumentType,DocumentTitle,DocumentDescription,TemplateCost,TemplateFileName,DocumentCategoryName,lt.IsEnabled,DocumentSubCategoryName,SubDocumentCategoryName FROM
	LogTemplateUpload lt JOIN DocumentCategory dc ON lt.DocumentCategory=dc.DocumentCategoryId
	JOIN DocumentSubCategory dsc ON lt.DocumentSubCategory=dsc.DocumentSubCategoryId
	JOIN DocumentSubSubCategory dss ON lt.DocumentSubSubCategory=dss.DocumentSubSubCategoryId
	WHERE LogId=@LogId 
	UNION
	SELECT TOP 1 [Action],ModifiedDate,DocumentType,DocumentTitle,DocumentDescription,TemplateCost,TemplateFileName,DocumentCategoryName,lt.IsEnabled,DocumentSubCategoryName,SubDocumentCategoryName FROM
	LogTemplateUpload lt JOIN DocumentCategory dc ON lt.DocumentCategory=dc.DocumentCategoryId
	JOIN DocumentSubCategory dsc ON lt.DocumentSubCategory=dsc.DocumentSubCategoryId
	JOIN DocumentSubSubCategory dss ON lt.DocumentSubSubCategory=dss.DocumentSubSubCategoryId
	WHERE LogId<@LogId 
END
ELSE
SELECT [Action],ModifiedDate,DocumentType,DocumentTitle,DocumentDescription,TemplateCost,TemplateFileName,DocumentCategoryName,lt.IsEnabled,DocumentSubCategoryName,SubDocumentCategoryName FROM
	LogTemplateUpload lt JOIN DocumentCategory dc ON lt.DocumentCategory=dc.DocumentCategoryId
	JOIN DocumentSubCategory dsc ON lt.DocumentSubCategory=dsc.DocumentSubCategoryId
	JOIN DocumentSubSubCategory dss ON lt.DocumentSubSubCategory=dss.DocumentSubSubCategoryId
	WHERE LogId=@LogId  
END

