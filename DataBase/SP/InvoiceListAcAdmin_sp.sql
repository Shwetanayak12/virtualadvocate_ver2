CREATE PROCEDURE InvoiceListAcAdmin_sp
@OrgId INT=NULL,
@UserId INT=NULL
AS
BEGIN

IF @OrgId IS NOT NULL
	SELECT  ISNULL(b.PaidStatus,0) As PaidStatus, b.InvoiceDocumentName,c.DocumentTitle ,a.CreatedDate,
	a.CustomerId,a.groupid, d.CustomerName
	FROM FilledTemplateDetails a left join InvoiceDetails b
	ON a.CustomerId=b.CustomerId and a.GroupId=b.GroupId
	JOIN DocumentTemplate c on a.TemplateId=c.TemplateId
	JOIN CustomerDetails d on a.CustomerId=d.CustomerId
	WHERE a.OrgId=@OrgId
	ORDER BY a.RowId desc
ELSE
	SELECT  ISNULL(b.PaidStatus,0) As PaidStatus, b.InvoiceDocumentName,c.DocumentTitle ,a.CreatedDate,
	a.CustomerId,a.groupid, d.CustomerName
	FROM FilledTemplateDetails a left join InvoiceDetails b
	ON a.CustomerId=b.CustomerId and a.GroupId=b.GroupId
	JOIN DocumentTemplate c on a.TemplateId=c.TemplateId
	JOIN CustomerDetails d on a.CustomerId=d.CustomerId
	WHERE a.UserId=@UserId
	ORDER BY a.RowId DESC
END