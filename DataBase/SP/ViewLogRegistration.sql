USE [VirtualAdvocate]
GO
ALTER PROCEDURE ViewLogRegistration 
 @LogId INT
  AS
  BEGIN
  DECLARE @ModifierName VARCHAR(150)
  DECLARE @RoleName VARCHAR(150)
   DECLARE @ModifierId INT
   DECLARE @RoleId INT
   DECLARE @UserId INT
  
	SET @ModifierId=(SELECT ModifierId FROM LogRegistrations WHERE LogId=@LogId)
	SET @UserId=(SELECT UserId FROM LogRegistrations WHERE LogId=@LogId)
	SET @RoleId=(SELECT RoleID FROM UserProfile WHERE UserId=@ModifierId)

	SET @RoleName=(SELECT RoleDescription FROM Roles WHERE RoleId=@RoleId)	
	print @RoleName
	SET @ModifierName= (SELECT FirstName+' - '+@RoleName AS ModifierName  FROM UserAddressDetails WHERE UserId=@ModifierId)
 
 IF EXISTS(SELECT Logid FROM LogRegistrations WHERE LogId<@LogId AND UserId=@UserId)
 BEGIN
	 SELECT [Action],ModifiedDate,FirstName, LastName,EmailAddress,LR.IsEnabled , LR.RoleId,OrgId,RoleDescription,orgName,LR.LogId,ISNULL(StreetName,'NA') AS StreetName,ISNULL(BuildingName,'NA') AS BuildingName,ISNULL(PlotNumber,'NA') AS PlotNumber,
	  ISNULL(BlockNumber,'NA') AS BlockNumber,ISNULL(Designation,'NA') AS Designation,ISNULL(Region,'NA') AS Region,ISNULL(LandMark,'NA') AS LandMark,LR.UserId,ModifierId,@ModifierName AS ModifierName
	  FROM LogRegistrations LR
	  JOIN OrganizationDetails OD ON LR.OrgId=OD.OrganizationId
	  JOIN Roles ON LR.RoleId=Roles.RoleId 
	  WHERE LogId = @LogId
	  UNION 

	  SELECT TOP 1 [Action],ModifiedDate,FirstName, LastName,EmailAddress,LR.IsEnabled , LR.RoleId,OrgId,RoleDescription,orgName,LR.LogId,ISNULL(StreetName,'NA') AS StreetName,ISNULL(BuildingName,'NA') AS BuildingName,ISNULL(PlotNumber,'NA') AS PlotNumber,
	  ISNULL(BlockNumber,'NA') AS BlockNumber,ISNULL(Designation,'NA') AS Designation,ISNULL(Region,'NA') AS Region,ISNULL(LandMark,'NA') AS LandMark,LR.UserId,ModifierId,@ModifierName AS ModifierName
	  FROM LogRegistrations LR
	  JOIN OrganizationDetails OD ON LR.OrgId=OD.OrganizationId
	  JOIN Roles ON LR.RoleId=Roles.RoleId 
	  WHERE LogId < @LogId AND LR.UserId=@UserId
	  ORDER BY ModifiedDate DESC
END
ELSE
 SELECT [Action],ModifiedDate,FirstName, LastName,EmailAddress,LR.IsEnabled , LR.RoleId,OrgId,RoleDescription,orgName,LR.LogId,ISNULL(StreetName,'NA') AS StreetName,ISNULL(BuildingName,'NA') AS BuildingName,ISNULL(PlotNumber,'NA') AS PlotNumber,
	  ISNULL(BlockNumber,'NA') AS BlockNumber,ISNULL(Designation,'NA') AS Designation,ISNULL(Region,'NA') AS Region,ISNULL(LandMark,'NA') AS LandMark,LR.UserId,ModifierId,@ModifierName AS ModifierName
	  FROM LogRegistrations LR
	  JOIN OrganizationDetails OD ON LR.OrgId=OD.OrganizationId
	  JOIN Roles ON LR.RoleId=Roles.RoleId 
	  WHERE LogId = @LogId

  

  END

