
ALTER PROCEDURE InvoiceList_sp
AS
BEGIN
select  ISNULL(b.PaidStatus,0) As PaidStatus, b.InvoiceDocumentName,c.DocumentTitle ,a.CreatedDate,
a.CustomerId,a.groupid, d.CustomerName

from FilledTemplateDetails a left join InvoiceDetails b
on a.CustomerId=b.CustomerId and a.GroupId=b.GroupId
join DocumentTemplate c on a.TemplateId=c.TemplateId
join CustomerDetails d on a.CustomerId=d.CustomerId

END