USE [VirtualAdvocate]
GO

/****** Object:  StoredProcedure [dbo].[CategoryInvoiceTotalAmount]    Script Date: 2/5/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CategoryInvoiceTotalAmount]
@UserId INT = NULL
AS
BEGIN
DECLARE @Orgid INT
IF @UserId IS NOT NULL
		BEGIN
			SET @Orgid=(SELECT OrganizationId FROM UserProfile WHERE UserId=@UserId)
			;WITH cte (Total,templateid)
			AS
			(
			SELECT COUNT(f.templateid) as Total,f.templateid from FilledTemplateDetails f
			join InvoiceDetails i ON f.CustomerId=i.CustomerId AND f.GroupId=i.GroupId
			WHERE f.Customerid IN(Select Customerid from CustomerDetails where OrganizationId=@Orgid)
			GROUP BY f.templateid
			)
			SELECT SUM(c.Total*dt.TemplateCost) AS InvoiceAmount,dc.DocumentCategoryName,dc.DocumentCategoryId FROM cte c
			JOIN DocumentTemplate dt ON  c.templateid=dt.TemplateId
			JOIN DocumentCategory dc ON dt.DocumentCategory=dc.DocumentCategoryId
			GROUP BY dc.DocumentCategoryName,dc.DocumentCategoryId	
		END
ELSE
BEGIN
	WITH cte (Total,templateid)
	AS
	(
	SELECT COUNT(f.templateid) as Total,f.templateid from FilledTemplateDetails f
	join InvoiceDetails i ON f.CustomerId=i.CustomerId AND f.GroupId=i.GroupId
	GROUP BY f.templateid
	)
	SELECT SUM(c.Total*dt.TemplateCost) AS InvoiceAmount,dc.DocumentCategoryName,dc.DocumentCategoryId FROM cte c
	JOIN DocumentTemplate dt ON  c.templateid=dt.TemplateId
	JOIN DocumentCategory dc ON dt.DocumentCategory=dc.DocumentCategoryId

	GROUP BY dc.DocumentCategoryName,dc.DocumentCategoryId
END

END


GO


