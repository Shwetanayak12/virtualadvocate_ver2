  CREATE PROCEDURE LogRegistrationList
  AS
  BEGIN
  SELECT [Action],ModifiedDate,FirstName+' '+LastName As Name, LR.RoleId,OrgId,RoleDescription,orgName,LR.LogId FROM LogRegistrations LR
  JOIN OrganizationDetails OD ON LR.OrgId=OD.OrganizationId
  JOIN Roles ON LR.RoleId=Roles.RoleId ORDER BY ModifiedDate DESC
  END

