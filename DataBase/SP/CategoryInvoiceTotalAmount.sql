USE [VirtualAdvocate]
GO
CREATE PROCEDURE CategoryInvoiceTotalAmount
@UserId INT = NULL
AS
BEGIN
DECLARE @Orgid INT
IF @UserId IS NOT NULL
		BEGIN
			SET @Orgid=(SELECT OrganizationId FROM UserProfile WHERE UserId=@UserId)
			;WITH CTE
				AS(
					SELECT DocumentCategoryId FROM DocumentCategory dc
					JOIN SelectedAccountServices ss ON dc.ServiceId=ss.ServiceId
					WHERE ss.UserId=48 AND DocumentCategoryId NOT IN(
							SELECT DT.DocumentCategory
							FROM FilledTemplateDetails FT
							JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
							JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId							
							AND FT.OrgId = 15
					) AND IsEnabled=1	
				)
				SELECT 0 As TotalCount,DC.DocumentCategoryName AS CategoryName FROM DocumentCategory DC
				JOIN CTE C ON DC.DocumentCategoryId = C.DocumentCategoryId
				UNION
			WITH cte (Total,templateid)
			AS
			(
			SELECT COUNT(f.templateid) as Total,f.templateid from FilledTemplateDetails f
			join InvoiceDetails i ON f.CustomerId=i.CustomerId AND f.GroupId=i.GroupId
			WHERE f.Customerid IN(Select Customerid from CustomerDetails where OrganizationId=@Orgid)
			GROUP BY f.templateid
			)
			SELECT SUM(c.Total*dt.TemplateCost) AS InvoiceAmount,dc.DocumentCategoryName,dc.DocumentCategoryId FROM cte c
			JOIN DocumentTemplate dt ON  c.templateid=dt.TemplateId
			JOIN DocumentCategory dc ON dt.DocumentCategory=dc.DocumentCategoryId
			GROUP BY dc.DocumentCategoryName,dc.DocumentCategoryId	
		END
ELSE
BEGIN
	WITH cte (Total,templateid)
	AS
	(
	SELECT COUNT(f.templateid) as Total,f.templateid from FilledTemplateDetails f
	join InvoiceDetails i ON f.CustomerId=i.CustomerId AND f.GroupId=i.GroupId
	GROUP BY f.templateid
	)
	SELECT SUM(c.Total*dt.TemplateCost) AS InvoiceAmount,dc.DocumentCategoryName,dc.DocumentCategoryId FROM cte c
	JOIN DocumentTemplate dt ON  c.templateid=dt.TemplateId
	JOIN DocumentCategory dc ON dt.DocumentCategory=dc.DocumentCategoryId

	GROUP BY dc.DocumentCategoryName,dc.DocumentCategoryId
END

END

CategoryInvoiceTotalAmount 48

WITH cte (Total,templateid)
			AS
			(
			SELECT COUNT(f.templateid) as Total,f.templateid from FilledTemplateDetails f
			join InvoiceDetails i ON f.CustomerId=i.CustomerId AND f.GroupId=i.GroupId
			WHERE f.Customerid IN(Select Customerid from CustomerDetails where OrganizationId=15)
			GROUP BY f.templateid
			)
			SELECT SUM(c.Total*dt.TemplateCost) AS InvoiceAmount,dc.DocumentCategoryName,dc.DocumentCategoryId FROM cte c
			JOIN DocumentTemplate dt ON  c.templateid=dt.TemplateId
			JOIN DocumentCategory dc ON dt.DocumentCategory=dc.DocumentCategoryId
			GROUP BY dc.DocumentCategoryName,dc.DocumentCategoryId	

			;WITH CTE
				AS(
					SELECT DocumentCategoryId FROM DocumentCategory dc
					JOIN SelectedAccountServices ss ON dc.ServiceId=ss.ServiceId
					WHERE ss.UserId=48 AND DocumentCategoryId NOT IN(
							SELECT DT.DocumentCategory
							FROM FilledTemplateDetails FT
							JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
							JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId							
							AND FT.OrgId = 15
					) AND IsEnabled=1	
				)
				SELECT 0 As TotalCount,DC.DocumentCategoryName AS CategoryName FROM DocumentCategory DC
				JOIN CTE C ON DC.DocumentCategoryId = C.DocumentCategoryId
				UNION
				SELECT 0 AS TotalCount ,DC.DocumentCategoryName AS CategoryName
				FROM FilledTemplateDetails FT
				JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
				RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
				AND FT.UserId=48
				GROUP BY DocumentCategoryName