USE [VirtualAdvocate]
GO
CREATE PROCEDURE GetCategoriesCurrnetMonthTotalCount
@CategoryType INT=NULL 
AS
BEGIN
IF @CategoryType=1
BEGIN
WITH CTE
AS(
	SELECT DocumentCategoryId  FROM DocumentCategory
	WHERE DocumentCategoryId NOT IN(
			SELECT DT.DocumentCategory
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
			WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
	) AND IsEnabled=1	
)
SELECT 0 As TotalCount,DC.DocumentCategoryName AS CategoryName FROM DocumentCategory DC
JOIN CTE C ON DC.DocumentCategoryId = C.DocumentCategoryId

UNION
SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
	FROM FilledTemplateDetails FT
	JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
	RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
	WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
	GROUP BY DocumentCategoryName

END
	


ELSE IF @CategoryType=2
BEGIN
WITH CTE
AS(
	SELECT DocumentSubCategoryId  FROM DocumentSubCategory 
	WHERE DocumentSubCategoryId NOT IN(
			SELECT DT.DocumentSubCategory
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
			WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
	) AND IsEnabled=1		
)
SELECT 0 As TotalCount,DC.DocumentSubCategoryName AS CategoryName FROM DocumentSubCategory DC
JOIN CTE C ON DC.DocumentSubCategoryId = C.DocumentSubCategoryId

UNION
	SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
	FROM FilledTemplateDetails FT
	JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
	RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
	WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
	GROUP BY DocumentSubCategoryName
	END
ELSE 
BEGIN
WITH CTE
AS(
	SELECT DocumentSubSubCategoryId  FROM DocumentSubSubCategory 
	WHERE DocumentSubSubCategoryId NOT IN(
			SELECT DT.DocumentSubCategory
			FROM FilledTemplateDetails FT
			JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
			RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
			WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
	) AND IsEnabled=1		
)
SELECT 0 As TotalCount,DC.SubDocumentCategoryName AS CategoryName FROM DocumentSubSubCategory DC
JOIN CTE C ON DC.DocumentSubSubCategoryId = C.DocumentSubSubCategoryId
UNION
	SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
	FROM FilledTemplateDetails FT
	JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
	RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
	WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
	GROUP BY SubDocumentCategoryName
	END
END

