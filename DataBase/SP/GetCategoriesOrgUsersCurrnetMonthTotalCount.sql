USE [VirtualAdvocate]
GO
CREATE PROCEDURE GetCategoriesOrgUsersCurrnetMonthTotalCount 
@CategoryType INT=NULL ,
@OrgId INT=NULL,
@UserId INT= NULL
AS
BEGIN
IF @CategoryType=1

BEGIN
	
		IF @OrgId IS NOT NULL
			BEGIN
			SET @UserId=(SELECT UserId FROM UserProfile WHERE OrganizationId=@OrgId AND RoleId=2)				
				;WITH CTE
				AS(
					SELECT DocumentCategoryId FROM DocumentCategory dc
					JOIN SelectedAccountServices ss ON dc.ServiceId=ss.ServiceId
					WHERE ss.UserId=@UserId AND DocumentCategoryId NOT IN(
							SELECT DT.DocumentCategory
							FROM FilledTemplateDetails FT
							JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
							RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
							WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
							AND FT.OrgId = @OrgId
					) AND IsEnabled=1	
				)
				SELECT 0 As TotalCount,DC.DocumentCategoryName AS CategoryName FROM DocumentCategory DC
				JOIN CTE C ON DC.DocumentCategoryId = C.DocumentCategoryId
				UNION
				SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
				FROM FilledTemplateDetails FT
				JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
				RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
				WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
				AND FT.OrgId=@OrgId
				GROUP BY DocumentCategoryName
			END
			ELSE
			BEGIN
				;WITH CTE
				AS(
					SELECT DocumentCategoryId FROM DocumentCategory dc
					JOIN SelectedAccountServices ss ON dc.ServiceId=ss.ServiceId
					WHERE ss.UserId=@UserId AND DocumentCategoryId NOT IN(
							SELECT DT.DocumentCategory
							FROM FilledTemplateDetails FT
							JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
							RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
							WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
							AND FT.UserId=@UserId
					) AND IsEnabled=1	
				)
				SELECT 0 As TotalCount,DC.DocumentCategoryName AS CategoryName FROM DocumentCategory DC
				JOIN CTE C ON DC.DocumentCategoryId = C.DocumentCategoryId
				UNION
				SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
				FROM FilledTemplateDetails FT
				JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
				RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
				WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
				AND FT.UserId=@UserId
				GROUP BY DocumentCategoryName
			END

END
	

ELSE IF @CategoryType=2
	
	BEGIN
		IF @OrgId IS NOT NULL
			BEGIN
				SET @UserId=(SELECT UserId FROM UserProfile WHERE OrganizationId=@OrgId AND RoleId=2)				
				;WITH CTE
				AS(
					SELECT ds.DocumentSubCategoryId FROM DocumentCategory dc
					JOIN SelectedAccountServices ss ON dc.ServiceId=ss.ServiceId
					JOIN DocumentSubCategory ds  ON dc.DocumentCategoryId=ds.DocumentCategoryId
					WHERE ss.UserId=@UserId AND 
					DocumentSubCategoryId NOT IN(
							SELECT DT.DocumentSubCategory
							FROM FilledTemplateDetails FT
							JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
							RIGHT JOIN DocumentsubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
							WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
							AND FT.OrgId = @OrgId
					) AND ds.IsEnabled=1	
				)
				SELECT 0 As TotalCount,DC.DocumentSubCategoryName AS CategoryName FROM DocumentSubCategory DC
				JOIN CTE C ON DC.DocumentSubCategoryId = C.DocumentSubCategoryId
				UNION
				SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
				FROM FilledTemplateDetails FT
				JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
				RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
				WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
				AND FT.OrgId=@OrgId
				GROUP BY DocumentSubCategoryName

				
			END
		ELSE
			BEGIN
				;WITH CTE
				AS(
					SELECT ds.DocumentSubCategoryId FROM DocumentCategory dc
					JOIN SelectedAccountServices ss ON dc.ServiceId=ss.ServiceId
					JOIN DocumentSubCategory ds  ON dc.DocumentCategoryId=ds.DocumentCategoryId
					WHERE ss.UserId=@UserId AND 
					DocumentSubCategoryId NOT IN(
							SELECT DT.DocumentSubCategory
							FROM FilledTemplateDetails FT
							JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
							RIGHT JOIN DocumentsubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
							WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
							AND FT.UserId=@UserId
					) AND ds.IsEnabled=1	
				)
				SELECT 0 As TotalCount,DC.DocumentSubCategoryName AS CategoryName FROM DocumentSubCategory DC
				JOIN CTE C ON DC.DocumentSubCategoryId = C.DocumentSubCategoryId
				UNION
				SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
				FROM FilledTemplateDetails FT
				JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
				RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
				WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
				AND FT.UserId=@UserId
				GROUP BY DocumentSubCategoryName
			END
	END
	

ELSE 
	
	BEGIN
		IF @OrgId IS NOT NULL
			BEGIN
			SET @UserId=(SELECT UserId FROM UserProfile WHERE OrganizationId=@OrgId AND RoleId=2)				
				;WITH CTE
				AS(
				SELECT dss.DocumentSubSubCategoryId FROM DocumentCategory dc
				JOIN SelectedAccountServices ss ON dc.ServiceId=ss.ServiceId
				JOIN DocumentSubCategory ds  ON dc.DocumentCategoryId=ds.DocumentCategoryId
				JOIN DocumentSubSubCategory dss ON ds.DocumentSubCategoryId=dss.DocumentSubCategoryId
				WHERE ss.UserId=@UserId AND 
					DocumentSubSubCategoryId NOT IN(
							SELECT DT.DocumentSubSubCategory
							FROM FilledTemplateDetails FT
							JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
							RIGHT JOIN DocumentsubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
							WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
							AND FT.OrgId =@OrgId
					) AND ds.IsEnabled=1	
				)
				SELECT 0 As TotalCount,DC.SubDocumentCategoryName AS CategoryName FROM DocumentSubSubCategory DC
				JOIN CTE C ON DC.DocumentSubSubCategoryId = C.DocumentSubSubCategoryId
				UNION
				SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
				FROM FilledTemplateDetails FT
				JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
				RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
				WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
				AND FT.OrgId=@OrgId
				GROUP BY SubDocumentCategoryName
			END
		ELSE
			BEGIN
			;WITH CTE
				AS(
				SELECT dss.DocumentSubSubCategoryId FROM DocumentCategory dc
				JOIN SelectedAccountServices ss ON dc.ServiceId=ss.ServiceId
				JOIN DocumentSubCategory ds  ON dc.DocumentCategoryId=ds.DocumentCategoryId
				JOIN DocumentSubSubCategory dss ON ds.DocumentSubCategoryId=dss.DocumentSubCategoryId
				WHERE ss.UserId=@UserId AND 
					DocumentSubSubCategoryId NOT IN(
							SELECT DT.DocumentSubSubCategory
							FROM FilledTemplateDetails FT
							JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
							RIGHT JOIN DocumentsubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
							WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
							AND FT.UserId=@UserId
					) AND ds.IsEnabled=1	
				)
				SELECT 0 As TotalCount,DC.SubDocumentCategoryName AS CategoryName FROM DocumentSubSubCategory DC
				JOIN CTE C ON DC.DocumentSubSubCategoryId = C.DocumentSubSubCategoryId
				UNION
				SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
				FROM FilledTemplateDetails FT
				JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
				RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
				WHERE FT.CreatedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0)
				AND FT.UserId=@UserId
				GROUP BY SubDocumentCategoryName
			END
	END
	

END

