ALTER PROCEDURE ViewLogSubSubCategory 
@LogId INT
AS
BEGIN

IF EXISTS(SELECT LogId FROM LogSubSubCategory WHERE LogId<@LogId)
BEGIN
	SELECT [Action],ModifiedDate,lc.DocumentSubSubCategoryId,SubSubCategoryDescription,SubDocumentCategoryName,lc.IsEnabled,DocumentSubCategoryName FROM
	LogSubSubCategory lc JOIN DocumentSubCategory dc ON lc.DocumentSubCategoryId=dc.DocumentSubCategoryId
	WHERE LogId=@LogId 
	UNION
	SELECT TOP 1 [Action],ModifiedDate,lc.DocumentSubSubCategoryId,SubSubCategoryDescription,SubDocumentCategoryName,lc.IsEnabled,DocumentSubCategoryName FROM
	LogSubSubCategory lc JOIN DocumentSubCategory dc ON lc.DocumentSubCategoryId=dc.DocumentSubCategoryId
	WHERE LogId<@LogId 
END
ELSE
SELECT [Action],ModifiedDate,lc.DocumentSubSubCategoryId,SubSubCategoryDescription,SubDocumentCategoryName,lc.IsEnabled,DocumentSubCategoryName FROM
	LogSubSubCategory lc JOIN DocumentSubCategory dc ON lc.DocumentSubCategoryId=dc.DocumentSubCategoryId
	WHERE LogId=@LogId 
END



