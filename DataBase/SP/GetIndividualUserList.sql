USE [VirtualAdvocate]
GO
CREATE PROCEDURE GetIndividualUserList
AS
BEGIN

SELECT up.UserID AS ID,ua.FirstName+' '+ua.LastName AS Name FROM UserProfile up
JOIN UserAddressDetails ua ON up.UserID=ua.UserId
WHERE up.RoleId=3 AND up.IsEnabled=1

END