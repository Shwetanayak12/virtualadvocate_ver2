CREATE PROCEDURE GraphMonthlyCompanyRegister
AS
BEGIN
SELECT 
  Company, 
  [1] AS January,
  [2] AS February,
  [3] AS March,
  [4] AS April,
  [5] AS May,
  [6] AS June,
  [7] AS July,
  [8] AS August,
  [9] AS September,
  [10] AS October,
  [11] AS November, 
  [12] AS December 
from
(
SELECT up.OrganizationId AS Total ,MONTH(up.CreatedDate) AS my_month,org.OrgName AS Company  FROM UserProfile up
RIGHT JOIN OrganizationDetails org ON up.OrganizationId=org.OrganizationId
where UserAccountsType=2 and YEAR(up.CreatedDate)=YEAR(getdate())-1 
--GROUP BY org.OrganizationId,MONTH(up.CreatedDate),org.OrgName
) AS t
PIVOT (

  COUNT(Total)
  FOR my_month IN([1], [2], [3], [4], [5],[6],[7],[8],[9],[10],[11],[12])
) as p

END
