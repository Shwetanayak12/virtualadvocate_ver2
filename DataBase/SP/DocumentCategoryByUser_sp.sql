USE [VirtualAdvocate]
GO
CREATE PROCEDURE DocumentCategoryByUser_sp
@UserID INT
AS
BEGIN

SELECT DC.DocumentCategoryId AS ID, DC.DocumentCategoryName AS Name FROM DocumentCategory DC
JOIN SelectedAccountServices SAS ON DC.ServiceId = SAS.ServiceId
WHERE SAS.UserId=@UserID

END

