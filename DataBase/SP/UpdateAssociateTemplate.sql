ALTER PROCEDURE UpdateAssociateTemplate
@TemplateId INT,
@AssociateTemplateId INT,
@Ordervalue INT
AS
BEGIN
IF EXISTS(SELECT 1,1 FROM AssociateTemplateDetails WHERE TemplateId=@TemplateId AND AssociateTemplateId=@AssociateTemplateId)
	BEGIN
		UPDATE AssociateTemplateDetails SET IsEnabled=1,DisplayOrder=@Ordervalue WHERE TemplateId=@TemplateId AND AssociateTemplateId=@AssociateTemplateId
	END
ELSE
	BEGIN
		INSERT INTO AssociateTemplateDetails(TemplateId,AssociateTemplateId,CreatedDate,IsEnabled,DisplayOrder)VALUES(@TemplateId,@AssociateTemplateId,GETDATE(),1,@Ordervalue) 
	END
END