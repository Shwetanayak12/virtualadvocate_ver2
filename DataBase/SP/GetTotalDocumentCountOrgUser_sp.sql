ALTER PROCEDURE GetTotalDocumentCountOrgUser_sp
@OrgId INT=NULL,
@UserId INT=NULL
AS
BEGIN
IF @OrgId IS NOT NULL
SELECT COUNT(*) AS TotalCount FROM FilledTemplateDetails
WHERE  OrgId=@OrgId 
ELSE  
SELECT COUNT(*) AS TotalCount FROM FilledTemplateDetails
WHERE UserId=@UserId
END