CREATE PROCEDURE ViewLogCategory 
@LogId INT
AS
BEGIN

IF EXISTS(SELECT LogId FROM LogDocumentCategory WHERE LogId<@LogId)
BEGIN
	SELECT [Action],ModifiedDate,DocumentCategoryId,DocumentCategoryDescription,DocumentCategoryName,lc.IsEnabled,ServicesDescription FROM
	LogDocumentCategory lc JOIN AccountServices acs ON lc.ServiceId=acs.ServiceId
	WHERE LogId=@LogId 
	UNION
	SELECT TOP 1 [Action],ModifiedDate,DocumentCategoryId,DocumentCategoryDescription,DocumentCategoryName,lc.IsEnabled,ServicesDescription FROM
	LogDocumentCategory lc JOIN AccountServices acs ON lc.ServiceId=acs.ServiceId
	WHERE LogId<@LogId 
END
ELSE
SELECT [Action],ModifiedDate,DocumentCategoryId,DocumentCategoryDescription,DocumentCategoryName,lc.IsEnabled,ServicesDescription FROM
LogDocumentCategory lc JOIN AccountServices acs ON lc.ServiceId=acs.ServiceId
WHERE LogId=@LogId 
END
