CREATE PROCEDURE NewEnquiriesDueDiligence_sp
@Month INT= NULL
AS
BEGIN
IF @Month IS NULL
	SELECT COUNT(enq.EnquiryType) AS newEQTotal, enqtyp.EnquiryType  FROM DueDiligenceEnquiry enq
	RIGHT JOIN DueDiligenceEnquiryType enqtyp ON enq.EnquiryType=enqtyp.EnquiryTypeId
	WHERE enq.ReplyStatus IS NULL
	GROUP BY enqtyp.EnquiryType
ELSE

	SELECT COUNT(enq.EnquiryType) AS newEQTotal, enqtyp.EnquiryType  FROM DueDiligenceEnquiry enq
	RIGHT JOIN DueDiligenceEnquiryType enqtyp ON enq.EnquiryType=enqtyp.EnquiryTypeId
	WHERE enq.ReplyStatus IS NULL AND MONTH(enq.CreatedDate) = MONTH(GETDATE())
	GROUP BY enqtyp.EnquiryType

END

