
CREATE PROCEDURE GetCategoriesTotalCount
@CategoryType INT=NULL 
AS
BEGIN
IF @CategoryType=1
	SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentCategoryName AS CategoryName
	FROM FilledTemplateDetails FT
	JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
	RIGHT JOIN DocumentCategory DC ON DT.DocumentCategory=DC.DocumentCategoryId
	GROUP BY DocumentCategoryName
ELSE IF @CategoryType=2
	SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.DocumentSubCategoryName AS CategoryName
	FROM FilledTemplateDetails FT
	JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
	RIGHT JOIN DocumentSubCategory DC ON DT.DocumentSubCategory=DC.DocumentSubCategoryId
	GROUP BY DocumentSubCategoryName
ELSE 
	SELECT COUNT(FT.TemplateId) AS TotalCount ,DC.SubDocumentCategoryName AS CategoryName
	FROM FilledTemplateDetails FT
	JOIN DocumentTemplate DT ON FT.TemplateId=DT.TemplateId
	RIGHT JOIN DocumentSubSubCategory DC ON DT.DocumentSubSubCategory=DC.DocumentSubSubCategoryId
	GROUP BY SubDocumentCategoryName
END
