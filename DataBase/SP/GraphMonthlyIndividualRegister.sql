USE [VirtualAdvocate]
GO
ALTER PROCEDURE GraphMonthlyIndividualRegister
AS
BEGIN
WITH CTE (Total,mymonth,morder)
AS(
SELECT COUNT(RoleId) AS Total, 'Jan' AS mymonth, 1 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 1
UNION
SELECT COUNT(RoleId) AS Total, 'Feb' AS mymonth, 2 AS morder FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 2
UNION
SELECT COUNT(RoleId) AS Total, 'Mar' AS mymonth, 3 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 3
UNION
SELECT COUNT(RoleId) AS Total, 'Apr' AS mymonth, 4 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 4
UNION
SELECT COUNT(RoleId) AS Total, 'May' AS mymonth, 5 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 5
UNION
SELECT COUNT(RoleId) AS Total, 'Jun' AS mymonth, 6 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 6
UNION
SELECT COUNT(RoleId) AS Total, 'Jul' AS mymonth, 7 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 7
UNION
SELECT COUNT(RoleId) AS Total, 'Aug' AS mymonth, 8 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 8
UNION
SELECT COUNT(RoleId) AS Total, 'Sep' AS mymonth, 9 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 9
UNION
SELECT COUNT(RoleId) AS Total, 'Oct' AS mymonth, 10 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 10
UNION
SELECT COUNT(RoleId) AS Total, 'Nov' AS mymonth, 11 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 11
UNION
SELECT COUNT(RoleId) AS Total, 'Dec' AS mymonth, 12 AS morder  FROM UserProfile 
WHERE RoleId=3 and YEAR(CreatedDate)=YEAR(getdate()) and  MONTH(CreatedDate) = 12
) 
SELECT * FROM CTE order by morder
END


