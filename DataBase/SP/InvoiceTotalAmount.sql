ALTER PROCEDURE InvoiceTotalAmount
@UserId INT = NULL
AS
BEGIN
DECLARE @Orgid INT
IF @UserId IS NOT NULL
BEGIN
SET @Orgid=(SELECT OrganizationId FROM UserProfile WHERE UserId=@UserId)
SELECT ISNULL(SUM(TotalAmount),0) AS  Amount FROM InvoiceDetails
WHERE Customerid IN(Select Customerid from CustomerDetails where OrganizationId=@Orgid)
END
ELSE
SELECT SUM(ISNULL(TotalAmount,0)) AS  Amount FROM InvoiceDetails

END

