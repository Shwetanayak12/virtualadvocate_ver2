CREATE PROCEDURE GetTotalDocumentCountByOrganization_sp
AS
BEGIN
SELECT OD.OrgName,COUNT(FT.OrgId) AS TotalDocumentCount FROM FilledTemplateDetails FT
RIGHT JOIN OrganizationDetails OD ON FT.OrgId=OD.OrganizationId
GROUP BY OrgName ORDER BY TotalDocumentCount DESC
END

