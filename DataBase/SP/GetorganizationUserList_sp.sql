CREATE PROCEDURE GetorganizationUserList_sp
@OrgId INT
AS
BEGIN
SELECT up.UserID,(ua.FirstName+' '+ua.LastName) AS Name
FROM UserProfile up
JOIN UserAddressDetails ua ON up.UserID=ua.UserId
WHERE up.OrganizationId=@OrgId
END
