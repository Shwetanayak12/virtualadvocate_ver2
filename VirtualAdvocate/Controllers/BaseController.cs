﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VirtualAdvocate.Controllers
{
    public class BaseController : Controller
    {
      
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if (Session["UserID"] == null)
            //{

            //    var url = new UrlHelper(filterContext.RequestContext);
            //    var loginUrl = url.Content("~/Login/Index");
            //    filterContext.Result = new RedirectResult(loginUrl);
            //    return;

            //}
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            var user = session["UserID"];
       
            if (((user == null) && (!session.IsNewSession)) || (session.IsNewSession))
            {
                //send them off to the login page
                var url = new UrlHelper(filterContext.RequestContext);
                var loginUrl = url.Content("~/Login/Index");
                session.RemoveAll();
                session.Clear();
                session.Abandon();
                filterContext.HttpContext.Response.Redirect(loginUrl, true);
            }
            //string actionName = filterContext.ActionDescriptor.ActionName;

            //if (filterContext.HttpContext.Request.IsAjaxRequest() && actionName.Equals("Login"))
            //{
            //    JsonResult jsonRes = new JsonResult();
            //    jsonRes.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            //    jsonRes.Data = "Session timeout. Redirecting...";

            //    filterContext.Result = jsonRes;
            //}

            base.OnActionExecuting(filterContext);
        }
    }
}