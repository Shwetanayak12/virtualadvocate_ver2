﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VirtualAdvocate.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Form()
        {
            ViewBag.Message = "Sample Form Page";

            return View();
        }

        public ActionResult Register1()
        {
            ViewBag.Message = "Form description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        //public static byte[] HtmlToWord(String html, string filePath)
        //{
        //    // const string filename = "test.docx";
        //    // string html = Properties.Resources.DemoHtml;

        //    //if (File.Exists(filename)) File.Delete(filename);

        //    using (MemoryStream generatedDocument = new MemoryStream())
        //    {
        //        using (WordprocessingDocument package = WordprocessingDocument.Create(generatedDocument, WordprocessingDocumentType.Document))
        //        {
        //            MainDocumentPart mainPart = package.MainDocumentPart;
        //            if (mainPart == null)
        //            {
        //                mainPart = package.AddMainDocumentPart();
        //                new DocumentFormat.OpenXml.Wordprocessing.Document(new Body()).Save(mainPart);
        //            }

        //            HtmlConverter converter = new HtmlConverter(mainPart);
        //            Body body = mainPart.Document.Body;

        //            var paragraphs = converter.Parse(html);
        //            for (int i = 0; i < paragraphs.Count; i++)
        //            {
        //                body.Append(paragraphs[i]);
        //            }

        //            mainPart.Document.Save();
        //        }

        //        return generatedDocument.ToArray();
        //    }
        //}
    }
}